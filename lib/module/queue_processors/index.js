'use strict';
const QueueProcessor = require('@axonite.io/axonite-core/lib/queue_processor');
const Service = require('../../../api').describeService();
const Aggregate = require('../aggregate/workflow_aggregate');

module.exports = {
  getQueueProcessors: function(){
    
    return [new QueueProcessor({
      queueName: Service.naturalKey,
      domainMessageHandlerParams: {handler: Aggregate}
    })];
  }
};