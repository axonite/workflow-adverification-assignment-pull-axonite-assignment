var LicenseModule = require('@axonite.io/axonite-license')
var Promie =require('bluebird')
var _ = require('lodash')
var logger = require('@axonite.io/axonite-core/lib/logger');

function License() {

}

License.prototype.updateOrCreate = function(currentSubscription,subscription) {
  var currentBudget = _.get(currentSubscription,"subscriptionForm.budget")
  var newbudget = _.get(subscription,"subscriptionForm.budget")

  logger.debug("new budget :",newbudget," current budget ",currentBudget)
  if (!currentBudget && !newbudget)
     return Promise.resolve()
  if (currentBudget && !newbudget)
     return this.delete(currentSubscription,subscription)

  var license = {organization:subscription.organization,name:subscription.id,
    budget: [{
      budgetType:newbudget.type ,
      budget:newbudget.value,
      timePeriod: newbudget.timePeriod}]}

  return LicenseModule.updateOrCreate(license)
}

License.prototype.delete = function(subscription) {

  var budget = _.get(subscription,"subscriptionForm.budget")
  if (!budget)
    return Promise.resolve()

  var license = {organization:subscription.organization,name:subscription.id}
  return LicenseModule.delete(license)
}

module.exports = new  License()