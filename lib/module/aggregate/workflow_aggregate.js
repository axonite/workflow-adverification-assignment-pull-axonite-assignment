'use strict';
const BaseWorkflow  = require('@axonite.io/axonite-workflow/lib/module/aggregate/base_workflow');
const Service = require('../../../api').describeService();
const AdVerification = require('@axonite.io/axonite-ad-verification');
const logger = require('@axonite.io/axonite-core/lib/logger');
const BU = require('@axonite.io/axonite-bu');
const promise = require('bluebird');

class AdVerificationWorkflow extends BaseWorkflow {
  constructor(opt) {
    var catalogChangefeedWorkflow = {
      service: Service,
      stages: {
        configureWorkflow: {
          autoExecute: false,
        }
      }
    };
    super(opt, catalogChangefeedWorkflow);
  }

  executeWorkflow(workflow) {
    logger.info(workflow.id, 'executeWorkflow: run AdVerification');
    return BU.get({id:workflow.bu})
      .then(bu=>{
        return AdVerification.execute(workflow, bu)
          .then(res => {
            logger.info(workflow.id, 'executeWorkflow: AdVerification summary', JSON.stringify(res));
            workflow.cycle.lastExecution = res;
            return workflow;
          });
      })
  }

  configureWorkflow(workflow, command) {
    logger.info(workflow.id, 'configureWorkflow: configure AdVerification');
    workflow.subscription.organization = command.organization;
    return promise.resolve(workflow);
  }
}


module.exports = AdVerificationWorkflow;