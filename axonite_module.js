'use strict';
const packageJson = require('./package.json');
const Service = require('./api').describeService();

class AxoniteModule {
  constructor(){
    this.name = packageJson.name;
    this.version = packageJson.version;

    this.Topics = {
      workflowEvents: 'workflowEvents'
    };
    this.Topics[Service.naturalKey] = Service.naturalKey;

    this.Queues = {
      workflowEvents: 'workflowEvents'
    };
    this.Queues[Service.naturalKey] = Service.naturalKey;
  }
  
  getCloudFormation(){
    var workflowCommands = Service.naturalKey;
    return [
      {
        tag: 'commands',
        queue: workflowCommands,
        subscribedTopics: [
          workflowCommands
        ]
      },
      {
        tag: 'events',
        queue: this.Queues.workflowEvents,
        subscribedTopics: [this.Topics.workflowEvents]
      }
    ];
  }

  getQueueProcessors() {
    return require('./lib/module/queue_processors').getQueueProcessors();
  }
}

module.exports = new AxoniteModule();