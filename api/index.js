'use strict';
var AxoniteModule = require('@axonite.io/axonite-core/lib/axonite_module');
var UniqueId      = require('@axonite.io/axonite-core/lib/unique_id_generator');
var promise       = require('bluebird');

module.exports = {
  initModule: function (opt) {
    return AxoniteModule.load(require('../axonite_module'), opt);
  },

  describeService: function(){
    var service = {
      scope: 'integration',
      provider: 'adVerification',
      providerModel:'assignment',
      customer:'axonite',
      customerModel: 'assignment',
      aspect : 'verification',
      model: 'trackingLink',
      method: 'pull',
      sla: 36000 //10 hours
    };
    service.naturalKey = [service.provider,service.providerModel,service.method,
      service.customer,service.customerModel,service.aspect].join('-');
    service.id = UniqueId.naturalKeyToGuid(service.naturalKey);
    service.queueProcessingMaxRate = 1;
    return service;
  },

  getServiceMeta: function(){
    return promise.resolve();
  }
};